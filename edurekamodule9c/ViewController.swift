//
//  ViewController.swift
//  edurekamodule9c
//
//  Created by Jay on 02/03/18.
//  Copyright © 2018 the chalakas. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController
{

    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //I just want to update the last cat in the collection
    @IBAction func updateCat()
    {
        print("updateCat function has started")
        
        //lets get a reference to the app delegate.
        let appDeleage = UIApplication.shared.delegate as! AppDelegate
        
        //first lets get the context.
        let context = appDeleage.persistentContainer.viewContext
        
        do
        {
            //lets get all the cats.
            let allTheCats = try context.fetch(Cat.fetchRequest())
            //lets get the position of the very last cat.
            let catToUpdatePosition = allTheCats.count - 1

            let catToUpdate = allTheCats[catToUpdatePosition] as! NSManagedObject
            
            //make the changes
            catToUpdate.setValue(10, forKey: "age")
            catToUpdate.setValue("New cat color", forKey: "color")
            
            //save the context i.e. db.
            appDeleage.saveContext()
        }
        catch
        {
            print("Cats have run away from the house. They are not the in the db man.")
        }
        
        print("updateCat function has ended")
    }
    
    //i just want to delete the last cat in DB.
    @IBAction func deleteCat()
    {
        print("deleteCat function has started")
        
        //lets get a reference to the app delegate.
        let appDeleage = UIApplication.shared.delegate as! AppDelegate
        
        //first lets get the context.
        let context = appDeleage.persistentContainer.viewContext

        do
        {
            //lets get all the cats.
            let allTheCats = try context.fetch(Cat.fetchRequest())
            //lets get the position of the very last cat.
            let catToDeletePosition = allTheCats.count - 1
            //delete cat at that position.
            context.delete(allTheCats[catToDeletePosition] as! NSManagedObject)
            //save the context i.e. db.
            appDeleage.saveContext()
        }
        catch
        {
            print("Cats have run away from the house. They are not the in the db man.")
        }
        
        print("deleteCat function has ended")
    }

    
    
    @IBAction func addCats()
    {
        print("addCats function has started")
        
        //lets get a reference to the app delegate.
        let appDeleage = UIApplication.shared.delegate as! AppDelegate
        
        //first lets get the context.
        let context = appDeleage.persistentContainer.viewContext
        //lets create a cat.
        let newCat = Cat(context: context)
        newCat.age = 5
        newCat.color = "Brown"
        
        //save the cat.
        appDeleage.saveContext()
        print("addCats function has ended")
    }

    @IBAction func showCats()
    {
        print("showCats function has started")
        
        //lets get a reference to the app delegate.
        let appDeleage = UIApplication.shared.delegate as! AppDelegate
        
        //first lets get the context.
        let context = appDeleage.persistentContainer.viewContext
        
        //lets get all the cats from the db.
        do
        {
            let allTheCats = try context.fetch(Cat.fetchRequest())
            
            //we cannot just print allTheCats from above. we need to convert them into usable format.
            
            //alright, lets create an array here.
            var allTheCatsArray = [Cat]()
            allTheCatsArray = allTheCats as! [Cat]
            for cat in allTheCatsArray
            {
                let index = allTheCatsArray.index(of: cat)
                print(index!)
                let CatDetails = "Cat Details - Age - " + String(cat.age) + " Color - " + cat.color!
                print(CatDetails)
            }
        }
        catch
        {
            print("Cats have run away from the house. They are not the in the db man.")
        }
        
        print("showCats function has ended")
    }

}

